const puppeteer = require('puppeteer');
const MongoClient = require('mongodb').MongoClient;
const cron = require('node-cron');
const parser = require('./libs/parser');

const mongoConnectUrl = "mongodb+srv://root:9vN4mTzvST8PbG0O@cluster0.goyau.mongodb.net/parser-data?retryWrites=true&w=majority";

let dbClient;
let browser;

(async () => {
    try {
        dbClient = await MongoClient.connect(mongoConnectUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        const db = dbClient.db("parser");

        browser = await puppeteer.launch({
            headless: true,
            args: [
                '--no-sandbox',
                '--disable-setuid-sandbox',
            ]
        });
        const page = await browser.newPage();
        await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');
        
        cron.schedule('0 0 * * *', async function() {
            await parser(page, db);
        });

    } catch (e) {

        console.error(e);

        if (dbClient) {
            await dbClient.close();
        }

        if (browser) {
            await browser.close();
        }
    
    }
})().catch(e => {
    console.error(e);
});