const httpBuildQuery = require('http-build-query');
const mainPage = require('./pages/main');
const categoryProducts = require('./pages/category-products');

const parser = async function(page, db) {

    //Start parser
    let startDate = new Date();

    let mainPageUrl = 'https://kaspi.kz/shop/';

    try {
        await db.collection('log').insertOne({'startDate': startDate});

        let categoryMenu = await mainPage(page, mainPageUrl);

        if (categoryMenu.error) {
            console.error(categoryMenu.result);

            let categoryMenu = await mainPage(page, mainPageUrl);

            if (categoryMenu.error) {
                throw categoryMenu.result;
            }
        }

        let count = 0;
        for (let c = 0; c < categoryMenu.result.length; c++) {
            let categoryUrl = categoryMenu.result[c].category.split(' ').join('%20');
            let curCategoryPage = 1;

            while (true) {
                let categoryMenuParam = {
                    page: curCategoryPage++,
                    sort: 'relevance',
                    q: `:category:${categoryUrl}`,
                };
                let categoryApiUrl = `https://kaspi.kz/shop/rest/misc/desktop/category/${categoryUrl}/results?` +
                    categoryUrl +
                    '&' +
                    httpBuildQuery(categoryMenuParam);

                let categoryProductsData = await categoryProducts(page, categoryApiUrl);

                if (categoryProductsData.error) {
                    console.error(categoryProductsData.result);
                    continue;
                }

                if (categoryProductsData.result.data.length === 0) {
                    break;
                }

                for (let p = 0; p < categoryProductsData.result.data.length; p++) {

                    let dataInsert = categoryProductsData.result.data[p];
                    dataInsert.dateAdded = new Date();

                    await db.collection('productsItemData').insertOne(dataInsert);
                }
            }
        }
    } catch (e) {
        console.error(e);

        await db.collection('log').insertOne({'error': e});

    } finally {
        let endDate = new Date();
        let parserWorkSecond = (endDate - startDate) / 1000;
        await db.collection('log').insertOne({'endDate': endDate});
        await db.collection('log').insertOne({'parserSpendTime': parserWorkSecond});
    }
}

module.exports = parser;