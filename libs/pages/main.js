const mainPage = async function(page, url) {
    try {
        await page.goto(url, {waitUntil: 'networkidle0'});

        let selector = 'a[data-city-id="750000000"]';
        if (page.$(selector) !== null) {
            page.click(selector);
            await page.waitForNavigation({waitUntil: 'networkidle0'});
            await page.goto(url);
            await page.waitForSelector('#page');
        }

        let result = await page.evaluate(async () => {
            return BACKEND.components.navMenu.links;
        });

        return {
            result: result,
            error: false
        }
    } catch (e) {
        return {
            result: e,
            error: true
        }
    }
};

module.exports = mainPage;