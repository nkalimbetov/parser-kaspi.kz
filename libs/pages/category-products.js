const categoryProducts = async function(page, url) {
    try {
        await page.goto(url, {waitUntil: 'networkidle0'});
        let result = await page.evaluate(async () => {
            return JSON.parse(document.querySelector('pre').innerText);
        });

        return {
            result: result,
            error: false
        }
    } catch (e) {
        return {
            result: e,
            error: true
        }
    }
};

module.exports = categoryProducts;