const productPage = async function(page, url) {
    try {
        let urlSplit = url.split('?');
        let offerUrl = urlSplit[0] + `offers/?c=${urlSplit[1].split('=')[1]}&limit=limitValue&page=pageValue&sort=asc`;

        await page.goto(url, {waitUntil: 'networkidle0', timeout: 50000});
        let result = await page.evaluate(async (productUrl, offerUrl) => {

            try {
                let title = document.querySelector('h1[class="item__heading"]').innerText;
                let sku = BACKEND.state.productId;
                let offers = Object.values(BACKEND.components.sellersOffers.components[0].preloadedOffers);
                let offersPageLimit = BACKEND.components.sellersOffers.components[0].limit;
                let offersTotal = BACKEND.components.sellersOffers.components[0].total;
                let requestCount = Math.ceil(offersTotal / offersPageLimit) - 1;

                for (let i = 1; i <= requestCount; i++) {
                    let offerUrlFetch = offerUrl.replace('pageValue', i).replace('limitValue', offersPageLimit);

                    let response = await fetch(offerUrlFetch);
                    let responseData = await response.json();

                    offers.push(...Object.values(responseData.data));
                }

                return {
                    item: {
                        _id: sku,
                        title: title,
                        minPrice: BACKEND.components.sellersOffers.minPrice,
                        maxPrice: BACKEND.components.sellersOffers.maxPrice,
                        offers: offers,
                    },
                    error: false
                };
            }
            catch(e) {
                return {
                    message: e,
                    error: true
                };
            }
        }, url, offerUrl);

        return result;
    } catch (e) {
		return {
            message: e,
			error: true
		};
    }
};


module.exports = productPage;